# README for SVN container
---
### Build command syntax
docker build \
--build-arg svn_user_authfile=<path-to-file-in-container> \
--build-arg svn_user=<username> \
--build-arg svn_passwd=<password> \
--build-arg svn_repo_name=<reponame> \
--build-arg svn_listen_port=3690 \
[-t <tagname> \]
.

This should build the docker image for you with the provided username and password as the svn user and the provided reponame as the name of the SVN repository that will be created.

### Create a docker volume
cd <some-folder>
docker volume create <volume-name>

### Run the container
#### from same folder as above
docker run -d -p 80:80 -p 3690:3690 -v <volume-name>:/var/www/svn --name <name-for-container> <tag-name-create-for-build>
